import cv2
import numpy as np
import math
import gtk, threading
import Queue
import ConfigParser as cfg
import time

my_cfg = cfg.ConfigParser()

my_cfg.read('config.cfg')
on_pi = my_cfg.get('Main', 'on_pi') == "True"

frames = Queue.Queue()
rawCapture = None

frameSkip = 12

lowerBound=np.array([33,80,40])
upperBound=np.array([102,255,20])

#font = cv2.cv.InitFont(cv2.cv.CV_FONT_HERSHEY_SIMPLEX,2,0.5,0,3,1)
font = cv2.FONT_HERSHEY_COMPLEX

lower_blue_rgb = np.uint8([[[32,32,64]]])
upper_blue_rgb = np.uint8([[[0,0,255]]])

lower_blue = cv2.cvtColor(lower_blue_rgb, cv2.COLOR_BGR2HSV)
upper_blue = cv2.cvtColor(upper_blue_rgb, cv2.COLOR_BGR2HSV)
#print lower_blue
#print upper_blue
# TODO
lower_orange = np.array([0, 96, 120])
upper_orange = np.array([25, 255, 255])

lower_yellow = np.array([25, 96, 120])
upper_yellow = np.array([60, 255, 255])

lower_green = np.array([50, 127, 96])
upper_green = np.array([100, 255, 255])

lower_blue = np.array([100,96,132])
upper_blue = np.array([150,255,255])

lower_red = np.array([170, 96, 120])
upper_red = np.array([220, 255, 255])

#lower_yellow = 
#upper_yellow = 
#lower_green = 
#upper_green = 
#lower_orange = 
#upper_orange =

class GtkThread():
    def __init__(self):
        #threading.Thread.__init__(self)
        print("Gtk thread start?")
        #thread = threading.Thread(target=self.run, args=())
        #self.daemon = True
        #thread.start()
        self.run()
        print("post thread start")
        
    def delete_event(self, widget, event, data=None):
        print("Delete event..")
        return False

    def lh_upd(self, value):
        print("Value updated: " + str(value))
        l_h = value

    def run(self):
        print("GTK thread")
        #import gobject
        #gobject.threads_init()

        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.connect("delete_event", self.delete_event)
        window.set_border_width(10)
       
        adj1 = gtk.Adjustment(0.0, 0.0, 255.0, 1.0, 1.0, 1.0)
        #adj1.connect("value_changed", self.lh_upd)
        hscale = gtk.HScale(adj1)
        hscale.set_size_request(300, 40)

        hscale.connect("value_changed", self.lh_upd)
        hscale.set_update_policy(gtk.UPDATE_CONTINUOUS)

        hscale.show()
        vbox = gtk.VBox()
        vbox.add(hscale)
        vbox.show()

        window.add(vbox)
        window.show()
        print("GTK main start")
        #gtk.main()
        #while gtk.events_pending:
        #    gtk.main_iteration()
        #print("Ended??")

#cam = cv2.VideoCapture(0)
kernelOpen=np.ones((5,5))
kernelClose=np.ones((20,20))

class OpenCVThread(threading.Thread):
    def __init__(self, ID):
        threading.Thread.__init__(self)
        self.daemon = True
        #thread = threading.Thread(target=self.run, args=())
        self.ID = ID
        self.cam = cv2.VideoCapture(ID)
        #thread.daemon = True
        #thread.start()
    
    def run(self):
        global frames
        global on_pi
        print("OpenCV thread")
        if not on_pi:
            while True:
                ret, frame=self.cam.read()
                frame=cv2.resize(frame,(480,320))
                frames.put(frame)
        else:
            '''global rawCapture
            from picamera.array import PiRGBArray
            from picamera import PiCamera
            camera = PiCamera()
            camera.resolution = (640, 480)
            camera.framerate = 32
            rawCapture = PiRGBArray(camera, size=(640, 480))
            time.sleep(0.1)

            for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	            # grab the raw NumPy array representing the image, then initialize the timestamp
	            # and occupied/unoccupied text
                image = frame.array
                print str(image)
                cv2.imshow("Frame", image)
                frames.put(image)
                rawCapture.truncate(0)'''
                

class MainApp(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True

    def run(self):
        global frames
        global on_pi
        print("Main thread")
        currentFrame = 0
        if on_pi:
            from picamera.array import PiRGBArray
            from picamera import PiCamera
            camera = PiCamera()
            camera.resolution = (640, 480)
            camera.framerate = 32
            rawCapture = PiRGBArray(camera, size=(640, 480))
            time.sleep(0.1)
        while True:
            frame = None
            currentFrame += 1
            
            if(not frames.empty() or on_pi):
                if not on_pi:
                    frame = frames.get()
                else:
                    #frame = io.BytesIO()
                    camera.capture(rawCapture, format="bgr")
                    # grab the raw NumPy array representing the image, then initialize the timestamp
                    # and occupied/unoccupied text
                    #frame.seek(0)
                    #print frame
                    frame = rawCapture.array
                    #print str(image)
                    #cv2.imshow("Frame", image)
                    
                #convert BGR to HSV
                hsv= cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
                
                # define range of blue color in HSV

                # Threshold the HSV image to get only blue colors
                #mask = cv2.inRange(hsv, lower_blue, upper_blue)
                mask = cv2.inRange(hsv, lower_yellow, upper_yellow)
                im2,conts,h=cv2.findContours(mask.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)

                cv2.drawContours(frame,conts,-1,(255,0,0),3)

                largest_rect = []
                largest_rect_size = 0
                largest_rect_i = 0

                for i in range(len(conts)):
                    x,y,w,h=cv2.boundingRect(conts[i])
                    size = math.sqrt(w * w + h * h)
                    
                    if size > largest_rect_size:
                        largest_rect_size = size
                        largest_rect_i = i
                        largest_rect = cv2.boundingRect(conts[i])
                
                if largest_rect_size > 0:
                    x,y,w,h = largest_rect
                    cv2.rectangle(frame,(x,y),(x+w,y+h),(0,0,255), 2)
                    #v2.cv.PutText(cv2.cv.fromarray(frame), str(i+1),(x,y+h),font,(0,255,255))

                # Bitwise-AND mask and original image
                res = cv2.bitwise_and(frame,frame, mask= mask)

                cv2.imshow('frame',frame)
                if on_pi:
                    rawCapture.truncate(0)

                if(currentFrame > frameSkip):
                    cv2.imwrite('images/latest-img.jpg', frame)
                    currentFrame = 0
                #cv2.imshow('mask',mask)
                #cv2.imshow('res',res)
                k = cv2.waitKey(5) & 0xFF
                if k == 27:
                    self.join()
                    break

'''
cvThread = OpenCVThread(0)
main = MainApp()

cvThread.start()
main.start()
#gtkThread.start()
#gtkThread = GtkThread()

cvThread.join()
main.join()
#gtkThread.join()


#gtk.main()
'''