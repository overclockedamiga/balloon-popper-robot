from flask import Flask, request, send_from_directory, send_file
import os, threading

app = Flask(__name__)

@app.route('/')
def hello_world():
    return send_file("index.html")

@app.route('/js/<path:filename>')
def send_js(filename):
    print("Sending " + filename)
    #root_dir = os.path.dirname(os.getcwd())
    #return send_from_directory(os.path.join(root_dir, 'js'), filename)
    return send_from_directory('./js/', filename)

@app.route('/css/<path:filename>')
def send_css(filename):
    #root_dir = os.path.dirname(os.getcwd())
    #return send_from_directory(os.path.join(root_dir, 'css'), filename)
    return send_from_directory('./css/', filename)

@app.route('/images/<path:filename>')
def send_img(filename):
    #root_dir = os.path.dirname(os.getcwd())
    #return send_from_directory(os.path.join(root_dir, 'css'), filename)
    return send_from_directory('./images/', filename)

@app.route('/answer')
def receive_answer():
    print(str(request.args.get('question')))
    #question_number = request.args.get('question')
    #answer = request.args.get('answer')
    return "example answer"

class WebServer(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True

    def run(self):
        global app
        app.run(host='0.0.0.0')