import ConfigParser as cfg

#import color_test
from web_server import WebServer
from color_test import MainApp
from color_test import OpenCVThread

my_cfg = cfg.ConfigParser()

my_cfg.read('config.cfg')
#print my_cfg.sections()
on_pi = my_cfg.get('Main', 'on_pi') == "True"
print(" is on pi? " + str(on_pi))

if on_pi is False:
    print "Running from PC"
else:
    print "Running on Pi"

ws = WebServer()
col_main = MainApp()
col_opencv = OpenCVThread(0)
ws.start()
col_main.start()
col_opencv.start()


ws.join()
col_main.join()
col_opencv.join()