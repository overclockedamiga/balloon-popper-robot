console.log("Hi");



setInterval(function(){
    $("#main-view").html("<img src=\"images/latest-img.jpg?" + new Date().getTime() + "\"/>");
}, 3000);

// premade questions - future ones to be random
var questions = ["5x5", "100/4", "11*11", "12*12", "2^4", "3*12"];
var answers   = ["25",  "25",    "121",   "144",  "32", "36"];

var a1 = $("#answer-1");
var a2 = $("#answer-2");
var a3 = $("#answer-3");
var a4 = $("#answer-4");

var currentAnswer = 0;

function nextQuestion(){
    var qnText = $("#question-field");

    // Random question
    var question_id = Math.floor(Math.random() * questions.length);
    qnText.html(questions[question_id]);

    // Random answer line
    var correct_n = Math.floor(Math.random() * 4);
    currentAnswer = correct_n;
    var answer_boxes = [a1, a2, a3, a4];
    var correct_answer_line = answer_boxes[correct_n];
    
    // set random incorrect answers (hopefully)
    for(var i = 0; i < 4; i++){
        answer_boxes[i].html(Math.floor(Math.random() * 150));
    }

    // Set random correct answer
    correct_answer_line.html(answers[question_id]);
}

nextQuestion();

function handleCorrectQuestion(){
    console.log("Correct!");
}

function checkQuestion(number){
    console.log("Selected: " + number + " correct: " + currentAnswer);
    if(number == currentAnswer){
        handleCorrectQuestion();
    } 
    nextQuestion();
}

// Answer 1
a1.mousedown(function(){
    a1.addClass('active');
    checkQuestion(0);
});

function clear_all(){
    a1.removeClass('active');
    a2.removeClass('active');
    a3.removeClass('active');
    a4.removeClass('active');
}

a1.mouseup(function(){
    clear_all()
});

// Answer 2
a2.mousedown(function(){
    a2.addClass('active');
    checkQuestion(1);
});

a2.mouseup(function(){
    clear_all();
});

// Answer 3
a3.mousedown(function(){
    a3.addClass('active');
    checkQuestion(2);
});

a3.mouseup(function(){
    clear_all();
});

// Answer 4
a4.mousedown(function(){
    a4.addClass('active');
    checkQuestion(3);
});

a4.mouseup(function(){
    clear_all();
});